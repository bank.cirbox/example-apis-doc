# Web-APIs-doc

## Environment
- Development
    - baseUrl: https://5460d3f9.ap.ngrok.io/front-end-test-api

## Revision History
| Version | Date | Changes Made | Author |
|-|-|-|-|
|0.1.0|2020-03-01|First Draft|BankDev|

---
## Apis

### Api Create
- Protocol : `HTTP`
- Path : `{baseUrl}/user`
- Method : `POST`
- Headers : `-`
- Request : `<application/json>`

    ```json
    {
        "name": "username",
        "role": "role",
        "email": "email@gmail.com",
        "phone": "0987654321"
    }
    ```

- Response : `<application/json>`

    ```json
    {
        "resCode": "0000",
        "resDesc": "success",
        "resData": {}
    }
    ```

### Api Read
- Protocol : `HTTP`
- Path : `{baseUrl}/user`
- Method : `GET`
- Headers : `-`
- Response : `<application/json>`

    ```json
    {
        "resCode": "0000",
        "resDesc": "success",
        "resData": [
            {
                "userId": "5x71xxx625xxx13xx4677543",
                "name": "username",
                "role": "role",
                "email": "email@gmail.com",
                "phone": "0987654321"
            }
        ]
    }
    ```
    
### Api Update
- Protocol : `HTTP`
- Path : `{baseUrl}/user/:{userId}`
- Method : `PUT`
- Headers : `-`
- Request : `<application/json>`

    ```json
    {
        "name": "username2",
        "role": "role2",
        "email": "email.2@gmail.com",
        "phone": "0912345678"
    }
    ```

- Response : `<application/json>`

    ```json
    {
        "resCode": "0000",
        "resDesc": "success",
        "resData": {}
    }
    ```
    
### Api Delete
- Protocol : `HTTP`
- Path : `{baseUrl}/user/:{userId}`
- Method : `DELETE`
- Headers : `-`
- Response : `<application/json>`

    ```json
    {
        "resCode": "0000",
        "resDesc": "success",
        "resData": {}
    }
    ```